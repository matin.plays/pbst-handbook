# 🏅 Points and ranking up

## How to earn points
Points can be earned in a few ways.

**_Trainings:_**

One way, and the most common way, to earn points is by attending a training. These trainings get announced on both the PBST discord as well as on our group wall.  Trainings can be hosted by:
 * Trainers,
 * Special Defense.

When attending a training, you can get anywhere between 0-5 points. How much points you receive depends on your performance and behaviour throughout the training. Different hosts has different ways to determine how well one does during a training.

Anyone is able to assist in a training, but it is up to the host on who will be an assistant. **Please do not ask to assist, this will simply lower your chances of you asssisting**.

For some trainings, such as mega trainings, you can get up to 10 points, depending on the same prinipals as normal trainings. To find out more about training, [click here](.../handbook/#training).

---

**_TMS Raids:_**

TMS raids are the most recommended way to earn points. If they start raiding a server to cause a melt- or freezedown, you can earn points quite easily by fighting against them. Make sure to keep an eye out for when they start joining your game.

The TMS raids have levels assigned to them, between 0 and 3. These determine the severity of the raid, and give an indication of how many TMS will be there. Level 3 raids (aka Mega raids) are the rarest, and involve TMS raiding multiple servers at once. TMS’ point system also depends on the raid level.

The PBST point system is independent of the TMS raid level, for all raids are to be responded to equally. PBST can earn up to 5 points if they’re present for the entire duration of the raid. Players with outstanding performance or leadership can earn up to 2 extra points. However, players who only attend part of the raid or perform poorly get less than 5 points.

An SD or Trainer will supervise the raid and monitor everyone’s performance to give points accordingly. They may do periodic checks to see who is attending on PBST’s side, so make sure you follow whatever instructions they give you. If no SD or Trainer is available, an Operative or higher from TMS will do the PBST attendance.

It is recommended you have Tier weapons or other good weapons, if the server is almost full and you’re trying to attend the raid with no weapons other than the baton, you may be removed from the game.

To find out more about TMS, scroll down as well as [clicking here](handbook/#the-mayhem-syndicate).

---

**_Self Training:_**

The last way to earn points is by doing self training. Self training allows you to practice your skills in a specific activity. For all activities, there are 5 different levels. The higher the level, the higher the points.

 To find out more about self training, [click here](#self-trainings).


:::danger A change to ST:
As of the 27th of June 2020, **self-training is locked to Tier 1+ only.**
![img](https://cdn.discordapp.com/attachments/726547736689770536/726547788854460501/unknown.png) 
:::

---

## Practice Trainings

Practice Training, also known as PTs, can be hosted by any PBST member at any facility (preferably PBSTAC or PBSTTF). 
These Trainings are *unofficial* and you won’t *earn Points in any way*. 

Normal PTs will usually consist of a few attendees with a Tier or Cadet hosting it. 
Before a PT can start, the person hosting the PT **must state that it’s not an official training and must explain that you won’t earn any points in any way.** 

You are not allowed to host a PT 30 minutes before an official Training (or 15 minutes with permission from the host).

This means that a PT must end around 15 to 30 minutes before an actual training takes place.

If a person claims to be a real trainer or Special Defense SD, take a screenshot and report the user to a HR.

---

## Ranking up

Once you reach certain amount of points, you'll be able to rank up. Below are the different ranks and how much points you need for each.

*Cadet - 0 points required (automatically earned).*

*Tier 1 - 100 points required.*

*Tier 2 - 250 points required.*

*Tier 3 - 500 points required.*

*Special Defense - 800 points required.*

**You'll need to do an evaluation for both tier 1 and Special Defense.** For tier 1 evaluations, you'll need to complete both a quiz section, as well as a patrol section in order to earn tier 1. For Special Defense SD evaluations, you'll need to host an evaluated training.

 For more information on both evaluations, please scroll below.


:::danger Keep in mind!
When you're a ``tier 1+``, you may be punished harder for mistakes you commit, you're supposed to be a role model for all the cadets and visitors in **Pinewood Facilities**. 
:::

---

## Tier 1 and Special Defense SD's evaluation

There are two different tier evaluation. This section explains both in a clear way.

**_Tier 1 evaluation_**

Once you reach 100 points, you must participate in an evaluation if you want to get the **Tier 1** rank. 
It is not recommended to get more points before your evaluation, if you take the evaluation while having more than *150 points **you’ll be set back** to 150*.

Scheduled Tier evaluations can be found on the PBST schedule at [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). 


There is a specialized server for Tier evaluations, use the command ``!pbstevalserver`` to get there. This only works if you have over 100 points.

A Tier 1 evaluation consists of 3 parts: a quiz, a combat evaluation, and a test on patrolling skills. The training rules will be heavily enforced in this evaluation, not following them will result in an immediate fail. If you pass one part but fail the other, you may try again at another evaluation and skip the part you passed before. (There is a database where we log all the people who failed and passed the quiz or the patrol).

During the quiz, you will receive 8 questions about various topics including this handbook and training rules. The questions will vary in difficulty, some are easy but others require more thinking. You need to score at least 5/8 to pass. Answering must be done privately through whisper chat or a Private Message system sent by the host.

:::warning Notice
A change was made to the SF Bot Challenge in the combat part of Tier evaluations by Trainer vote:
![img](https://cdn.discordapp.com/attachments/699908783861923952/790341353057026058/unknown.png)
Now when attempting the SF Bot Challenge you will have a **7-second cooldown** and **200 health**.
:::

During the combat eval, you will face 3 combat-related challenges: complete a Level 1 SF Bot Arena alone, survive Bomb Barrage with increasing levels, and score as many hits as possible in the Firing Range within 60 seconds. Each completed challenge will give points, as presented in this chart:

![img](https://cdn.discordapp.com/attachments/459764670782504961/790341171607371776/unknown.png)

During the patrol test, you will receive Tier 1 loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. A number of **Tier 3**'s will work against you in this part, as they will try to bring the core to melt- or freezedown.

---


**_Special Defense evaluation_**

Tier 3s who reach 800 points are eligible for an SD evaluation to become Special Defense. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “``Passed SD Eval``” until the Trainers choose to promote you to Special Defense 4 special defense. Having Discord is required.

As a Special Defense, you receive the ability to place a KoS in Pinewood facilities, you can order room restrictions, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used **responsibly**.

Special Defense SDs can host trainings with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Special Defense SDs may not request a training more than 3 days ahead of time. There also has to be a 1-hour gap at least between the end of one training and the start of another.

---

## Trainer
To become a Trainer, all the current Trainers have to vote on your promotion. Only Special Defense SDs are eligible for this promotion.

Trainers are the leadership of the group; they handle operations, promotions, points, and are responsible for all changes done to the group. They can host trainings without any of the SD’s restrictions.

:::danger Do not ask for promotions
We can't say it enough, do not ask for points, promotions or anything related to this. You are to earn points like everyone does, and these points define what rank you will be in the group. Trainers and SDs have the authority to deduct your points if they deem it necessary. Wait for the points to be logged, and earn your rank like everyone else.
:::

---

## When will points be logged?
Points doesn't get automatically logged. Each point is logged manually by a trainer. Points being logged depends on which trainer is availible and has the time to log points. **DO NOT ask trainers for points to be logged**. This will only be annoying for the trainers logging the points in question. By spamming the trainers about when points will be logged, ***you're only putting yourself in trouble.*** They have the ability to **SUBTRACT** your points.


**If you have any further questions and enquiries about this section, feel free to ask in the PBST discord on the #handbook-questions channel. (*DISCLAIMER: You must be 13 or over*).**

